package cs475;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class EvenOddPredictor extends Predictor implements Serializable {
    

    @Override
    public void train(List<Instance> instances) {
        // no need to train
        return;
    }

    @Override
    public Label predict(Instance instance) {
        FeatureVector feature_vector = instance.getFeatureVector();
        Set<Integer> vector_keys = feature_vector.getFeatures();
        double even_sum = 0.0;
        double odd_sum = 0.0;
        for (Integer key : vector_keys) {
            double attribute_value = feature_vector.get(key);
            if (key % 2 == 0) {
                even_sum += attribute_value;
            } else {
                odd_sum += attribute_value;
            }
        }
        int label = 0;
        if (even_sum >= odd_sum) {
            label = 1;
        }
        return new ClassificationLabel(label);
    }
}


