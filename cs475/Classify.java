
package cs475;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;

public class Classify {
	static public LinkedList<Option> options = new LinkedList<Option>();
	
	public static void main(String[] args) throws IOException, AlgorithmNotFoundException {
		// Parse the command line.
		String[] manditory_args = { "mode"};
		createCommandLineOptions();
		CommandLineUtilities.initCommandLineParameters(args, Classify.options, manditory_args);
	
		String mode = CommandLineUtilities.getOptionValue("mode");
		String data = CommandLineUtilities.getOptionValue("data");
		String predictions_file = CommandLineUtilities.getOptionValue("predictions_file");
		String algorithm = CommandLineUtilities.getOptionValue("algorithm");
		String model_file = CommandLineUtilities.getOptionValue("model_file");
                int sgd_iterations = 20;
                if (CommandLineUtilities.hasArg("sgd_iterations")) {
                    sgd_iterations = CommandLineUtilities.getOptionValueAsInt("sgd_iterations");
                }
                double sgd_eta0 = 0.01;
                if (CommandLineUtilities.hasArg("sgd_eta0")) {
                    sgd_eta0 = CommandLineUtilities.getOptionValueAsFloat("sgd_eta0");
                }
                double pegasos_lambda = 1e-4;
                if (CommandLineUtilities.hasArg("pegasos_lambda")) {
                    pegasos_lambda = CommandLineUtilities.getOptionValueAsFloat("pegasos_lambda");
                }
		
		if (mode.equalsIgnoreCase("train")) {
			if (data == null || algorithm == null || model_file == null) {
				System.out.println("Train requires the following arguments: data, algorithm, model_file");
				System.exit(0);
			}
			// Load the training data.
                        DataReader data_reader = new DataReader(data, true);
			List<Instance> instances = data_reader.readData();
			data_reader.close();
			
			// Train the model.
			Predictor predictor = train(instances, algorithm, sgd_iterations, sgd_eta0, pegasos_lambda);
			saveObject(predictor, model_file);		
			
		} else if (mode.equalsIgnoreCase("test")) {
			if (data == null || predictions_file == null || model_file == null) {
				System.out.println("Train requires the following arguments: data, predictions_file, model_file");
				System.exit(0);
			}
			
			// Load the test data.
			DataReader data_reader = new DataReader(data, true);
			List<Instance> instances = data_reader.readData();
			data_reader.close();
			
			// Load the model.
			Predictor predictor = (Predictor)loadObject(model_file);
                        if (predictor == null) {
                            System.exit(1);
                        }
			evaluateAndSavePredictions(predictor, instances, predictions_file);
		} else {
			System.out.println("Requires mode argument.");
		}
	}
	

	private static Predictor train(List<Instance> instances, String algorithm, int iterations, double eta0, double pegasos_lambda) throws AlgorithmNotFoundException {
                Predictor predict = null;
                if (algorithm.equalsIgnoreCase("majority")) {
                    predict = new MajorityPredictor();
                    predict.train(instances);
                } else if (algorithm.equalsIgnoreCase("even_odd")) {
                    predict = new EvenOddPredictor();
                    predict.train(instances);
                } else if (algorithm.equalsIgnoreCase("logistic_regression")) {
                    predict = new LogisticRegressionPredictor(iterations, eta0);
                    predict.train(instances);
                } else if (algorithm.equalsIgnoreCase("pegasos")) {
                    predict = new SVMPredictor(iterations, pegasos_lambda);
                    predict.train(instances);
                } else {
                    throw new AlgorithmNotFoundException();
                }
                     
		return predict;
	}

	private static void evaluateAndSavePredictions(Predictor predictor,
			List<Instance> instances, String predictions_file) throws IOException {
		PredictionsWriter writer = new PredictionsWriter(predictions_file);

                boolean is_labeled = true;
                // check to see if data had labels
                // if not, then we cannot classify accuracy
                for (Instance instance: instances) {
                    if (instance.getLabel() == null) {
                        is_labeled = false;
                        break;
                    }
                }
                // Compute and print out identical statement
                // as found in compute_accuracy.py
                if (is_labeled) {

                    Evaluator eval = new AccuracyEvaluator();
                    double accuracy = eval.evaluate(instances, predictor);
                    int num_correct = eval.return_num_correct();
                    int num_total = eval.return_num_total();
                    System.out.println("Accuracy: " + accuracy + " (" + num_correct + "/" + num_total + ")");
                } else {
                  
                    System.out.println("Data not labeled, no statistics output can be provided");
                }
                // actually write out predictions
		for (Instance instance : instances) {
			Label label = predictor.predict(instance);
			writer.writePrediction(label);
		}
		
		writer.close();
		
	}

	public static void saveObject(Object object, String file_name) {
		try {
			ObjectOutputStream oos =
				new ObjectOutputStream(new BufferedOutputStream(
						new FileOutputStream(new File(file_name))));
			oos.writeObject(object);
			oos.close();
		}
		catch (IOException e) {
			System.err.println("Exception writing file " + file_name + ": " + e);
		}
	}

	/**
	 * Load a single object from a filename. 
	 * @param file_name
	 * @return
	 */
	public static Object loadObject(String file_name) {
		ObjectInputStream ois;
		try {
			ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(file_name))));
			Object object = ois.readObject();
			ois.close();
			return object;
		} catch (IOException e) {
			System.err.println("Error loading: " + file_name);
		} catch (ClassNotFoundException e) {
			System.err.println("Error loading: " + file_name);
                } 
		return null;
	}
	
	public static void registerOption(String option_name, String arg_name, boolean has_arg, String description) {
		OptionBuilder.withArgName(arg_name);
		OptionBuilder.hasArg(has_arg);
		OptionBuilder.withDescription(description);
		Option option = OptionBuilder.create(option_name);
		
		Classify.options.add(option);		
	}
	
	private static void createCommandLineOptions() {
		registerOption("data", "String", true, "The data to use.");
		registerOption("mode", "String", true, "Operating mode: train or test.");
		registerOption("predictions_file", "String", true, "The predictions file to create.");
		registerOption("algorithm", "String", true, "The name of the algorithm for training.");
		registerOption("model_file", "String", true, "The name of the model file to create/load.");
                registerOption("sgd_eta0", "double", true, "The constant scalar for learning ratein AdGrad.");
                registerOption("sgd_iterations", "int", true, "The number of SGD iterations."); 
                registerOption("pegasos_lambda", "double", true, "The regularization parameter for pegasos");
		
		// Other options will be added here.
	}
}
