// Darian Hadjiabadi
// MachL Assignment 2

package cs475;

import java.io.Serializable;

import java.util.List;
import java.util.ArrayList;

import java.util.HashMap;

import java.util.Set;


public class LogisticRegressionPredictor extends Predictor implements Serializable {
    
    private int iterations; // passed in via constructor
    private double eta0; // passed in via constructor

    private List<HashMap<Integer, Double>> eta_matrix = null;
    private HashMap<Integer, Double>  parameter_weights = null;
    private HashMap<Integer, Double> squared_gradient_history = null;

    private void init(List<Instance> instances) {
        // init data structures

        this.eta_matrix = new ArrayList<HashMap<Integer, Double>>();
        for (int i = 0; i < instances.size(); i++) {
            this.eta_matrix.add(i, new HashMap<Integer, Double>());
        }

        this.squared_gradient_history = new HashMap<Integer, Double>();
        this.parameter_weights = new HashMap<Integer, Double>();

        // get all the keys in training data
        for (Instance init_inst : instances) {
            FeatureVector feature_vector = init_inst.getFeatureVector();
            Label label = init_inst.getLabel();
            Set<Integer> feature_keys = feature_vector.getFeatures();

            for (Integer key : feature_keys) {
                this.parameter_weights.put(key, 0.0);
            }
            
        }
    }
    // clear squared history?
    private void clear_history(int rows) {
        //this.squared_gradient_history = new HashMap<Integer, Double>();
    }
    // constructor
    public LogisticRegressionPredictor(int iterations, double eta0) {
        this.iterations =iterations;
        this.eta0 = eta0;
    }

    @Override
    public void train(List<Instance> instances) {
        this.init(instances); // initialize various structures and acquire keys
        for (int i = 0; i < this.iterations; i++) { // iterate
            //System.out.println("iteration #: " + (i+1));
            int j = 0;
            this.clear_history(instances.size());
            for (Instance instance : instances) { // step
                FeatureVector feature_vector = instance.getFeatureVector();
                Label label = instance.getLabel();
                this.step_adagrad(label, feature_vector, j);
                j += 1;
            }
        }
        return;
    }

    @Override
    public Label predict(Instance instance) {
        // acquire w^{T}*x and send through logistic function to determine probability
        // Classify as 0 if g(w^{T}*x) < 0.5 else classify as 1
        FeatureVector feature_vector = instance.getFeatureVector();
        Set<Integer> feature_keys = feature_vector.getFeatures();
        double dot_product = this.weighted_sum(feature_vector);
        double logistic_evaluation = this.logisticlinkevaluation(dot_product);
        if (logistic_evaluation < 0.5) {
            return new ClassificationLabel(0);
        }
        return new ClassificationLabel(1);
    }
 
    // a step through training instance
    private void step_adagrad(Label label, FeatureVector feature_vector, int step_number) {
        int int_lab = Integer.parseInt(label.toString());
        Set<Integer> feature_keys = feature_vector.getFeatures();
        HashMap<Integer, Double> weight_gradient = new HashMap<Integer, Double>(); // key: feature number; value: feature gradient

        double neg_link = this.logisticlinkevaluation(-1 * this.weighted_sum(feature_vector));
        double link = this.logisticlinkevaluation(this.weighted_sum(feature_vector));

        for (Integer key : feature_keys) { // go through all the features in the vector
            double grad = int_lab*neg_link*feature_vector.get(key) + (1 - int_lab)*link*(-1*feature_vector.get(key));
            weight_gradient.put(key, grad); // update gradient datastructure
            if (!this.squared_gradient_history.containsKey(key)) {
                this.squared_gradient_history.put(key,grad*grad);
            } else {
                double curr_sq_grad_history = this.squared_gradient_history.get(key);
                this.squared_gradient_history.put(key, curr_sq_grad_history + grad*grad); // update f
            }
        }

        for (Integer key : feature_keys) {
            double param_weight = this.parameter_weights.get(key); // acquire w
            double param_eta = this.update_eta(key); // update eta
            this.eta_matrix.get(step_number).put(key, param_eta); // put the updated eta in the eta matrix
            double updated_weight = param_weight + param_eta*weight_gradient.get(key); // update: w' = w + eta*grad_{wj}(l(Y,X,w))
            this.parameter_weights.put(key, updated_weight); // update the data structure holding parameter weights
        }
    }

    private double update_eta(int key) {
        double history = this.squared_gradient_history.get(key);
        return (this.eta0) / Math.sqrt(1 + history);
    }

    // essentially computes w^{T}*x given x vector and current parameter weights
    private double weighted_sum(FeatureVector feature_vector) {

        double dot_product = 0.0;
        Set<Integer> feature_keys = feature_vector.getFeatures();
        for (Integer key : feature_keys) {
            double value = feature_vector.get(key);
            double feature_weight = 0.0;
            if (this.parameter_weights.containsKey(key)) { 
                feature_weight = this.parameter_weights.get(key);
            }
            dot_product += (value * feature_weight);
        }
        return dot_product;
    }

    private double logisticlinkevaluation(double input) {
        return 1/(1+Math.exp(-1*input));   
    }
}

