// Darian Hadjiabadi
// MachL Assignment 3

package cs475;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.HashMap;




public class SVMPredictor extends Predictor implements Serializable {


    private int iterations;
    private double pegasos_lambda;

    private HashMap<Integer, Double> parameter_weights;

    private void init(List<Instance> instances) {
        
        this.parameter_weights = new HashMap<Integer, Double>();

        for (Instance instance : instances) {
            FeatureVector fv = instance.getFeatureVector();
            Set<Integer> feature_keys = fv.getFeatures();
            for (Integer key : feature_keys) {
                this.parameter_weights.put(key,0.0);
            }
        }
    }

    public SVMPredictor(int iterations, double pegasos_lambda) {
        this.iterations = iterations;
        this.pegasos_lambda = pegasos_lambda;
    }
    
    public void train(List<Instance> instances) {
        
        this.init(instances);
        int time_step = 1;
        for (int i = 0; i < this.iterations; i++) {
             for (Instance instance : instances) {
                 int label = Integer.parseInt(instance.getLabel().toString());
                 if (label == 0) {
                     label = -1;
                 }
                 double w_dot_x = this.weighted_sum(instance.getFeatureVector());
                 double p1 = label * w_dot_x;
                 int hinge = this.hinge_loss(p1);
                 int p2 = hinge*label;
                 this.pegasos_update(p2,instance.getFeatureVector(), time_step);
                 time_step += 1;
            
            }
        } 
        return;
    }
    public Label predict(Instance instance) {
        double x_dot_w = this.weighted_sum(instance.getFeatureVector());
        if (x_dot_w >= 0.0) {
            return new ClassificationLabel(1);
        }
        return new ClassificationLabel(0);
    }

    private void pegasos_update(int p2, FeatureVector fv, int time_step) {
        HashMap<Integer, Double> updated_weights = new HashMap<Integer, Double>();
        Set<Integer> weight_keys = this.parameter_weights.keySet();
        Set<Integer> fv_keys = fv.getFeatures();

        for (Integer wk : weight_keys) {
            double curr_weight = this.parameter_weights.get(wk);
            double updated_weight = 0.0;
            if (fv_keys.contains(wk)) {
                updated_weight = (1-(1/(double)time_step))*curr_weight + (1/(this.pegasos_lambda*(double)time_step))*p2*fv.get(wk);
            } else {
                updated_weight = (1-(1/(double)time_step))*curr_weight;
            }
            updated_weights.put(wk, updated_weight);
        }
        this.parameter_weights = updated_weights;
    }
             
    private int hinge_loss(double val) {
        if (val < 1.0) {
            return 1;
        }
        return 0;
    }
    private double weighted_sum(FeatureVector fv) {
        double dot_product = 0.0;
        Set<Integer> feature_keys = fv.getFeatures();
        for (Integer key : feature_keys) {
            double value = fv.get(key);
            double feature_weight = 0.0;
            if (this.parameter_weights.containsKey(key)) {
                feature_weight = this.parameter_weights.get(key);
            }
            dot_product += (value * feature_weight);
        }
        return dot_product;   
    }

}
