
package cs475.loopMRF;

public class LoopyBP {

	private LoopMRFPotentials potentials;
	private int iterations;

	double[][][] factor_to_variable_message;
	double[][][] variable_to_factor_message;
	

	public LoopyBP(LoopMRFPotentials p, int iterations) {
		this.potentials = p;
		this.iterations = iterations;
		factor_to_variable_message =new double[potentials.loopLength()*2+1][potentials.loopLength()+1][potentials.numXValues()];
		variable_to_factor_message =new double[potentials.loopLength()+1][potentials.loopLength()*2+1][potentials.numXValues()];
		
	}

	public double[] marginalProbability(int x_i) {

	    factor_to_variable_message =new double[potentials.loopLength()*2+1][potentials.loopLength()+1][potentials.numXValues()];

	    variable_to_factor_message =new double[potentials.loopLength()+1][potentials.loopLength()*2+1][potentials.numXValues()];
            double[] marginal = new double[potentials.numXValues()+1];
	    for (int k = 0; k < potentials.numXValues(); k++) {
	        variable_to_factor_message[1][potentials.loopLength()+1][k] = 1.0;
	        variable_to_factor_message[1][potentials.loopLength()*2][k] = 1.0;
	    }


   	    for (int t = 0; t < iterations; t++) {
                for (int i = 1; i <= potentials.loopLength(); i++) {
		    int a = potentials.loopLength() + i;
		    int b = 1 + i%potentials.loopLength();
		    for (int j = 0; j < potentials.numXValues(); j++) {
			double sum = 0.0;
			for (int k = 0; k < potentials.numXValues(); k++) {
			    sum += (variable_to_factor_message[i][a][k]*potentials.potential(a,j+1,k+1));
		            //factor_to_variable_message[a][b][j] += (variable_to_factor_message[i][a][k]*potentials.potential(a,j+1,k+1));
			}
			factor_to_variable_message[a][b][j] += sum;
			  
		    }

		    int c = potentials.loopLength()+1+i%potentials.loopLength();
		    for (int j = 0; j < potentials.numXValues(); j++) {
			
		        variable_to_factor_message[b][c][j] += (potentials.potential(b,j+1)*factor_to_variable_message[a][b][j]);
		    }
		   
		}
		for (int i = potentials.loopLength(); i >= 1; i--) {
		    int a = potentials.loopLength() + i;
		    int b = i;
		    
		    for (int j = 0; j < potentials.numXValues(); j++) {
			double sum = 0;
			for (int k = 0; k < potentials.numXValues(); k++) {
			    //factor_to_variable_message[a][b][j] += (variable_to_factor_message[b][a][k]*potentials.potential(a,j+1,k+1));
			    sum += (variable_to_factor_message[b][a][k]*potentials.potential(a,j+1,k+1));
			}
			factor_to_variable_message[a][b][j] += sum;
		    }
		    int c = potentials.loopLength()+1+(i-2+potentials.loopLength())%potentials.loopLength();;
	            for (int j = 0; j < potentials.numXValues(); j++) {
		        variable_to_factor_message[b][c][j] += (potentials.potential(b,j+1)*factor_to_variable_message[a][b][j]);
		    } 

		}
            } 

		
	    int a;
	    if (x_i == 1) { a = 2*potentials.loopLength(); } else { a = potentials.loopLength()+x_i-1; }
	    int b = x_i;
	    int c = x_i + potentials.loopLength();
		
	    for (int j = 0; j < marginal.length-1; j++) {
	        marginal[j+1] = (factor_to_variable_message[a][x_i][j]*factor_to_variable_message[c][x_i][j]*potentials.potential(x_i,j+1));
	    }

	    // Turn into probability distribution
	    double sum = 0.0;
	    for (int i = 0; i < marginal.length; i++) {
                sum += marginal[i];
	    }
            for (int i = 0; i < marginal.length; i++) {
               marginal[i] = marginal[i] / sum;
	    }
	    return marginal;
	}

}

