package cs475;

import java.io.Serializable;

public class RegressionLabel extends Label implements Serializable {

        private double label;
	public RegressionLabel(double label) {
                this.label = label;
	}

	@Override
	public String toString() {
                System.out.println(this.label);
		return String.valueOf(this.label);
	}

}
