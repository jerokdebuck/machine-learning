package cs475;

import java.io.Serializable;
import java.util.List;

public class MajorityPredictor extends Predictor implements Serializable {

    private int label_zero_count = 0;
    private int label_one_count = 0;

    @Override
    public void train(List<Instance> instances) {
        // for each instance, check the label. add as necessary
        for (Instance inst : instances) {
            Label label = inst.getLabel();
            String label_string = label.toString();
            if (label_string.equals("0")) {
                label_zero_count += 1;
            } else if (label_string.equals("1")) {
                label_one_count += 1;
            }
        }
    }

    @Override
    public Label predict(Instance instance) {
        if (this.label_zero_count > this.label_one_count) {
            return new ClassificationLabel(0);
        }
        return new ClassificationLabel(1);
    }

}
