// Darian Hadjiabadi
// MachL HW 4

package cs475.RBM;

public class RBMEnergy {
	private RBMParameters _parameters;
	private int _iters;
	private double _eta;

        // tables containing all possible x,h
        double[][] h_possibilities;
        double[][] x_possibilities;
        // data structures for storing gradients
        double[][] weight_gradient;
        double[] visible_bias_gradient;
        double[] hidden_bias_gradient;
        // Z
        double partition_value;
	
        // initialize parameters and create table
        // of possible h and x values
        private void init() {

            for (int j = 0; j < _parameters.numHiddenNodes(); j++) {
                if (j%2 == 1) { _parameters.setHiddenBias(j,1.0);}
            }
            for (int i = 0; i < _parameters.numVisibleNodes(); i++) {
                if (i % 2 == 1) { 
                    _parameters.setVisibleBias(i,1.0);
                }
                for (int j = 0; j < _parameters.numHiddenNodes(); j++) {
                    if (j % 2 == 1) {
                        _parameters.setWeight(i,j,1.0);
                    }
                }
            }
            h_possibilities = new double[(int)Math.pow(2,_parameters.numHiddenNodes())][_parameters.numHiddenNodes()];
            for (int i = 0; i < Math.pow(2,_parameters.numHiddenNodes()); i++) {
                for (int j = _parameters.numHiddenNodes()-1; j >= 0; j--) {
                    h_possibilities[i][j] = (i/(int)Math.pow(2,j))%2;
                }
            }
            x_possibilities = new double[(int)Math.pow(2,_parameters.numVisibleNodes())][_parameters.numVisibleNodes()];
            for (int i = 0;i < Math.pow(2,_parameters.numVisibleNodes()); i++) {
                for (int j = _parameters.numVisibleNodes() -1; j >= 0; j--) {
                    x_possibilities[i][j] = (i/(int)Math.pow(2,j))%2;
                }
            }
                    
        }

        /* Caclulate Z = sum(x,h) e^(-E(x,h))
           where E(x,h) = -x^(T)Wh - bx - d^(T)h
        */
        private void calculatePartition() {
            partition_value = 0.0;
            for (int t = 0; t < Math.pow(2,_parameters.numVisibleNodes()); t++) {
                double[] example= this.getPossibleInput(t);
                for (int u = 0; u < Math.pow(2,_parameters.numHiddenNodes()) ; u++) {
                    double[] h = this.getHidden(u);
                    double energy = this.calculateEnergy(example,h);
                    partition_value += Math.exp(-1*energy);
                }
            }
        }

        // Calculate E(x,h) given x and h
        private double calculateEnergy(double[] example, double[] h) {
	    double x_sum = 0.0;            
            for (int i = 0; i < _parameters.numVisibleNodes();i++) {
                x_sum += (_parameters.getVisibleBias(i)*example[i]);
            }
            double h_sum = 0.0;
            for (int j = 0; j < _parameters.numHiddenNodes(); j++) {
                h_sum += (_parameters.getHiddenBias(j)*h[j]);
            }
            double weight_sum = 0.0;
            for (int j = 0; j < _parameters.numHiddenNodes(); j++) { 
                double h_j = h[j];
                for (int i = 0; i < _parameters.numVisibleNodes(); i++) { 
                    double x_i = example[i];
                    double w_ij = _parameters.getWeight(i,j);
                    weight_sum += (x_i*w_ij*h_j);
                }
            }
                  // partition_value += Math.exp(x_sum +h_sum +weight_sum);
            return (-1*x_sum - weight_sum - h_sum);
                
        }

	public RBMEnergy(RBMParameters parameters, int iters, double eta) {
		this._parameters = parameters;
		this._iters = iters;
		this._eta = eta;
	}
	
	public void learning() {
            this.init();
            for (int i = 0; i < _iters; i++) {
                // calculate new Z for each iteration using updated
                // model parameters
                this.calculatePartition();
                //System.out.println("Partition value: " + partition_value);
                // reinitialize gradient data structures to 0.0
                weight_gradient = new double[_parameters.numVisibleNodes()][_parameters.numHiddenNodes()];
                visible_bias_gradient = new double[_parameters.numVisibleNodes()];
                hidden_bias_gradient = new double[_parameters.numHiddenNodes()];
		/* weightGradientUpdate, visibleBiasGradientUpdate, hiddenBiasGradientUpdate all part of the E-step */

                /*double logl = 0.0;
                for (int t = 0; t < _parameters.numExamples(); t++) { 
                    logl += this.calculateLogLikelihood(this.getExample(t));
                }
                System.out.println("Log likelihood: " + logl);*/

                this.weightGradientUpdate();
                this.hiddenBiasGradientUpdate();
                this.visibleBiasGradientUpdate();

                /* M-step */
                this.maximizationUpdate();
            }
	}

        // Calculates p(x(t);theta) - used for debugging
        private double calculateLogLikelihood(double[] example) {
                double h_energy = 0.0;
                for (int j = 0; j < Math.pow(2,_parameters.numHiddenNodes()); j++) {
                    double[] hidden = this.getHidden(j);
                    double energy = this.calculateEnergy(example,hidden);
                    h_energy += (Math.exp(-1*h_energy/partition_value));
                 }
                 return Math.log(h_energy);
         }

        /* Maximization Step: theta^(k) = theta^(k-1)+(eta*d/d(theta^(k-1)) [sum log marginal probability] */
        private void maximizationUpdate() {
            for (int j =0; j < _parameters.numHiddenNodes(); j++) {
                double hiddenBias = _parameters.getHiddenBias(j);
                _parameters.setHiddenBias(j,hiddenBias+(_eta*hidden_bias_gradient[j]));
            }
            for (int i = 0; i < _parameters.numVisibleNodes(); i++) {
                 double visibleBias = _parameters.getVisibleBias(i);
                _parameters.setVisibleBias(i, visibleBias + (_eta*visible_bias_gradient[i]));
                for (int j = 0; j < _parameters.numHiddenNodes(); j++) {
                    double weight = _parameters.getWeight(i,j);
                    _parameters.setWeight(i,j,weight + (_eta*weight_gradient[i][j]));
                }
            }
        }

        // Acquire gradient for weights w(i,j)
        private void weightGradientUpdate() {
            for (int i = 0; i < _parameters.numVisibleNodes(); i++) {
                for (int j = 0; j < _parameters.numHiddenNodes(); j++) {
                    double weight_conditional_expectation = 0.0;
                    double[] weight_column = this.getWeightColumn(j);
                    double hidden_bias = _parameters.getHiddenBias(j);
                    // conditional_expectation = sum(i=1:t)(sigma(dj+x(t)*W(,-j))*x(t)[i])
                    for (int t = 0; t < _parameters.numExamples(); t++) {
                        double example = _parameters.getExample(t,i);
                        double[] input = this.getExample(t);
                        double input_dot_weight = this.dotProduct(input,weight_column,input.length);
                        weight_conditional_expectation +=(this.logisticLinkEvaluation(input_dot_weight+hidden_bias)*example);
                    }
                    double joint_expectation = this.JointDistribution(i,j,1,1);
                    weight_gradient[i][j] += (weight_conditional_expectation - (_parameters.numExamples() * joint_expectation));
                }
            }
        }



	// Calculates p(x(-i),h(-j),x[i]=1,h[j]=1
        private double JointDistribution(int i,int j, int ui, int uj) { 
            double joint_expectation = 0.0;
            for (int t = 0; t < Math.pow(2,_parameters.numVisibleNodes()); t++) {
                double[] example =this.getPossibleInput(t);
                if (ui == 1 && example[i] != 1.0) { continue; }
                for (int u = 0; u < Math.pow(2,_parameters.numHiddenNodes()); u++) {
                    double[] hidden = this.getHidden(u);
                    if (uj == 1 && hidden[j] != 1.0) { 
                        continue;
                    }
                    double energy = this.calculateEnergy(example,hidden);
                    joint_expectation += (Math.exp(-1*energy)/partition_value);
  	        } 
            }
            return joint_expectation;
        }

        private void hiddenBiasGradientUpdate() {
            for (int j = 0; j < _parameters.numHiddenNodes(); j++) {
                double hidden_bias_conditional_expectation = 0.0;
                double[] weight_column = this.getWeightColumn(j);
                double hidden_bias = _parameters.getHiddenBias(j);
                // conditional_expectation = sum(i=1 to T) (sigma(x(t)*W(-,j))
                for (int t = 0; t < _parameters.numExamples(); t++) {
                    double[] input = this.getExample(t);
                    double input_dot_weight = this.dotProduct(input,weight_column,input.length);
                    hidden_bias_conditional_expectation += (this.logisticLinkEvaluation(input_dot_weight + hidden_bias));
                }
                double hidden_joint_expectation = this.JointDistribution(-1,j,0,1);      
                hidden_bias_gradient[j] += ((hidden_bias_conditional_expectation - (_parameters.numExamples() * hidden_joint_expectation)));
            }
                
        }
        private void visibleBiasGradientUpdate() {
           
           for (int i = 0; i < _parameters.numVisibleNodes(); i++) {
               double visible_conditional_expectation = 0.0;
               // conditional expectation = sum(i=1:T) (x(t)[i])
               for (int t = 0; t < _parameters.numExamples(); t++) {
                   
                   visible_conditional_expectation += (this.getExample(t)[i]);
                
               }
               double visible_joint_expectation = this.JointDistribution(i,-1,1,0);
               visible_bias_gradient[i] += ((visible_conditional_expectation - (_parameters.numExamples()*visible_joint_expectation)));
           }
       }
                       
	double[] getExample(int position) {
            double[] input = new double[_parameters.numVisibleNodes()];
            for (int i = 0; i < _parameters.numVisibleNodes(); i++) {
                input[i] = _parameters.getExample(position,i);
            }
            return input;

        }
        double[] getHidden(int position) {
            double[] hidden= new double[_parameters.numHiddenNodes()];
            for (int i = 0; i < _parameters.numHiddenNodes(); i++) {
                hidden[i] = h_possibilities[position][i];
            }
            return hidden;
        }
        double[] getPossibleInput(int position) {
            double input[] = new double[_parameters.numVisibleNodes()];
            for (int i = 0; i < _parameters.numVisibleNodes(); i++) {
                input[i] = x_possibilities[position][i];
            }
            return input;
        }
        double[] getWeightColumn(int col) {
            double[] weight_column = new double[_parameters.numVisibleNodes()];
            for (int i = 0; i < _parameters.numVisibleNodes(); i++) {
                weight_column[i] = _parameters.getWeight(i,col);
            }
            return weight_column;
            

        }
        double[] getWeightRow(int row) {
            double[] weight_row = new double[_parameters.numHiddenNodes()];
            for (int i = 0; i < _parameters.numHiddenNodes(); i++) {
                weight_row[i] = _parameters.getWeight(row,i);
            }
            return weight_row;
        }
        double dotProduct(double[] x, double[] y, int length) {
            double sum =0.0;
            for (int i = 0;  i < length; i++) {
                sum += (x[i]*y[i]);
            }
            return sum;
        }
        private double logisticLinkEvaluation(double input) {
            return 1/(1+Math.exp(-1*input));
        }

	public void printParameters() {
		//NOTE: Do not modify this function
		for (int i=0; i<_parameters.numVisibleNodes(); i++)
			System.out.println("b_" + i + "=" + _parameters.getVisibleBias(i));
		for (int i=0; i<_parameters.numHiddenNodes(); i++)
			System.out.println("d_" + i + "=" + _parameters.getHiddenBias(i));
		for (int i=0; i<_parameters.numVisibleNodes(); i++)
			for (int j=0; j<_parameters.numHiddenNodes(); j++)
				System.out.println("W_" + i + "_" + j + "=" + _parameters.getWeight(i,j));
	}
}
