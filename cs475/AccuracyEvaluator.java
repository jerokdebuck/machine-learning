package cs475;

import java.util.List;

public class AccuracyEvaluator extends Evaluator {

    private int num_total = 0;
    private int num_correct = 0;

    @Override
    public double evaluate(List<Instance> instances, Predictor predictor) {
        for (Instance inst : instances) {
            String str_given_label = inst.getLabel().toString();
            String str_pred_label = predictor.predict(inst).toString();
            this.num_total += 1;
            if (str_given_label.equals(str_pred_label)) {
                this.num_correct += 1;
            }
        }
        return (double)this.num_correct/this.num_total; 
    }
    @Override
    public int return_num_correct() { return this.num_correct; }
    @Override
    public int return_num_total() { return this.num_total; }   
}
