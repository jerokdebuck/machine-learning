package cs475;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

public class FeatureVector implements Serializable {

        private HashMap<Integer, Double> feature_map = new HashMap<Integer,Double>();

	public void add(int index, double value) {
                this.feature_map.put(index,value);
		
	}
	
	public double get(int index) {
                if (this.feature_map.containsKey(index)) {
		    return this.feature_map.get(index);
                }
                System.out.println("This print should never occur");
                return 0.0;
	}
        // return the attribute (HashMap keys)
        public Set<Integer> getFeatures() {
            return this.feature_map.keySet();
        }

}
