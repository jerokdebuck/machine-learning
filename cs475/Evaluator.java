package cs475;

import java.util.List;

public abstract class Evaluator {

	public abstract double evaluate(List<Instance> instances, Predictor predictor);
        public abstract int return_num_correct();
        public abstract int return_num_total();
}
