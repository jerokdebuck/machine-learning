
#include "Label.h"
#include "ClassificationLabel.h"
#include "FeatureVector.h"

#ifndef INSTANCE_H
#define INSTANCE_H


class Instance {

    private:
        Label * label = NULL;
        FeatureVector feature_vector;

    public:
        Instance(FeatureVector, Label*);
        ~Instance();
        Label* getLabel();
        void setLabel(Label*);
        FeatureVector getFeatureVector();
        void setFeatureVector(FeatureVector);
};




#endif
