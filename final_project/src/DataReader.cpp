#include "DataReader.h"
#include "FeatureVector.h"
#include "ClassificationLabel.h"
#include "Label.h"
#include "Instance.h"

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <sstream>
#include <vector>

using std::string;
using std::stringstream;
using std::vector;

DataReader::DataReader(char * file_name) {
    this -> file_name = file_name;
}

DataReader::~DataReader() {

    
}


char * DataReader::ReturnFileName(void) {
    return this -> file_name;
}

vector<Instance> DataReader::ReadData(void) {
    vector<Instance> instances;
    std::ifstream data_file(this->file_name);
    if (!data_file.is_open()) {
        printf("Data could not be read\n");
        return instances;
    }
    string line;
    while (getline(data_file, line)) {
        FeatureVector feature_vector;

        /* Split string on white space */
        vector<string> external;
        char ex_delim = ' ';
        stringstream ex_ss(line);
        string ex_tok;
        while (getline(ex_ss,ex_tok,ex_delim)) {
            external.push_back(ex_tok);
        }
        string label_string = external[0];
        int int_label = stoi(label_string);
        Label * label = new ClassificationLabel(int_label);
        
        for (unsigned int i = 1; i < external.size(); i++) {
            vector<string> internal;
            stringstream in_ss(external[i]);
            
            char in_delim = ':';
            string in_tok;
            while (getline(in_ss, in_tok, in_delim)) {
                internal.push_back(in_tok);
            }
            string feature_name = internal[0];
            int int_feature_name = stoi(feature_name);
            string feature_value = internal[1];
            double double_feature_value = stod(feature_value);
            if (double_feature_value != 0.0) {
                feature_vector.add(int_feature_name, double_feature_value);
            }
        }
        Instance instance(feature_vector, label);
        instances.push_back(instance); 
    }

    data_file.close();
    return instances;
}
