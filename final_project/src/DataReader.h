#include <iostream>
#include "FeatureVector.h"
#include "Instance.h"
#include <vector>

using std::vector;

#ifndef DATAREADER_H
#define DATAREADER_H


class DataReader {

    private:
        char * file_name;

    public:
        DataReader(char *);
        ~DataReader();
        char * ReturnFileName(void);
        vector<Instance> ReadData(void);

};


#endif //DATAREADER_H
