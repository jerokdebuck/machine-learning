
#ifndef PREDICTOR_H
#define PREDICTOR_H

#include "Label.h"
#include "Instance.h"
#include <vector>

class Predictor {
    
    public:
        virtual void train(std::vector<Instance>) = 0;
        virtual Label* predict(Instance instance) = 0;

};

#endif
