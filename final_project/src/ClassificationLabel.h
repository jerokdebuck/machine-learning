

#ifndef CLASSIFICATIONLABEL_H
#define CLASSIFICATIONLABEL_H

#include "Label.h"
#include <string>
using std::string;

class ClassificationLabel : public Label {
    private:
        int label;
    public:
        ClassificationLabel(int);
        ~ClassificationLabel();
        
        string toString();

};



#endif
