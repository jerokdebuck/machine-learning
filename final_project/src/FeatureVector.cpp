
#include "FeatureVector.h"
#include <vector>
#include <stdio.h>
#include <map>

using std::vector;

FeatureVector::FeatureVector() {

}

FeatureVector::~FeatureVector() {}

void FeatureVector::add(int index, double value) {
    this->feature_map[index] = value;
}

double FeatureVector::get(int index) {
    return this->feature_map[index];
}

vector<int> FeatureVector::getFeatures() {
    vector<int> key_vector;
    for (std::map<int,double>::iterator it = this->feature_map.begin(); it != this->feature_map.end(); ++it) {
        key_vector.push_back(it->first);
    } 
    return key_vector;
}


