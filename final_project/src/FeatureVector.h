
#include <vector> 
#include <map>

#ifndef FEATUREVECTOR_H
#define FEATUREVECTOR_H


class FeatureVector {

    private:
        std::map<int,double> feature_map;

    public:
        FeatureVector();
        ~FeatureVector();
        void add(int, double);
        double get(int);
        std::vector<int> getFeatures(void);


};


#endif
