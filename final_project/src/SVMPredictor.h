

#ifndef SVMPREDICTOR_H
#define PREDICTOR_H

#include "Predictor.h"
#include "Instance.h"
#include "Label.h"

#include <vector>
#include <map>
#include <fstream>
#include <iostream>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

class SVMPredictor : public Predictor {

    private:
        int iterations = 20;
        double pegasos_lambda = 1e-4;

    public:
        std::map<int,double> param_weights;
        //int param_weights = 0;
        SVMPredictor();
        SVMPredictor(int,double);
        ~SVMPredictor();
        void train(std::vector<Instance>);
        Label * predict(Instance);


};


namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive & ar, SVMPredictor & svm, const unsigned int version) {
   ar & svm.param_weights;
}
}
}



#endif
