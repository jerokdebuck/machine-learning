

#ifndef MAJORITYPREDICTOR_H
#define MAJORITYPREDICTOR_H

#include "Label.h"
#include "Instance.h"
#include "Predictor.h"

#include <vector>
#include <fstream>
#include <iostream>

using std::vector;

class MajorityPredictor: public Predictor {
   // private:

    public:
        int label_zero_count = 0;
        int label_one_count = 0;

        MajorityPredictor();
        ~MajorityPredictor();
        void train(vector<Instance>);
        Label* predict(Instance);

        int get_zero_count(void);
        int get_one_count(void);

        void set_zero_count(int);
        void set_one_count(int);


        friend std::ostream& operator<<(std::ostream& os, const MajorityPredictor& mp) {
            os << mp.label_zero_count << '\n';
            os << mp.label_one_count;
            return os;
        }
        friend std::istream& operator>>(std::istream& is, MajorityPredictor& mp) {
            is >> mp.label_zero_count >> mp.label_one_count;
            return is;
        }
};


#endif
