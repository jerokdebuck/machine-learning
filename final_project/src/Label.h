
#ifndef LABEL_H
#define LABEL_H

#include <string>
using std::string;

class Label {
    public:
        virtual string toString() = 0;

};



#endif
