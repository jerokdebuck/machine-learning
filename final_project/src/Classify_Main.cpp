#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <fstream>
#include <iostream>

#include "DataReader.h"
#include "Instance.h"
#include "Predictor.h"
#include "MajorityPredictor.h"
#include "SVMPredictor.h"

using std::vector;

typedef struct {
    char * data;
    char * mode;
    char * model_file;
    char * algorithm;
    int iterations = 20;
    double pegasos_lambda = 1e-4;
} CLInputs;


/* Function prototypes */
///////////////////////////////////////////////
void help_syntax(void);
void read_opts(CLInputs*, int, char *[]);
void train(Predictor*, vector<Instance>);
void evaluateAndSavePredictions(Predictor*, vector<Instance>);
void free_labels(vector<Instance>);
///////////////////////////////////////////////

void free_labels(vector<Instance> instances) {
    for (unsigned int i = 0; i < instances.size(); i++) {
        Label * label = instances[i].getLabel();
        free(label);
    }

}

void evaluateAndSavePredictions(Predictor * predictor, vector<Instance> instances) {
    int total = instances.size();
    int num_correct = 0;
    for (int i = 0; i < total; i++) {
        string str_given_label = instances[i].getLabel()->toString();
        string str_pred_label = predictor->predict(instances[i])->toString();
        int int_given_label = stoi(str_given_label);
        int int_pred_label = stoi(str_pred_label);
        if (int_pred_label == int_given_label) {
            num_correct += 1;
        }
    }
    double accuracy = (double) num_correct / total;
    printf("Accuracy: %f\t(%d,%d)\n",accuracy,num_correct,total);

}
        

void train(Predictor * predictor, vector<Instance> instances) {
    predictor->train(instances);

}
void read_opts(CLInputs * cl_inputs, int argc, char * argv[]) {
    int nopts = 1;
    while (nopts < argc && argv[nopts][0] == '-') {
        bool found_arg = false;
        if (!strcmp(argv[nopts], "-help")) {
            help_syntax();
            exit(1);
        }
        if (strncasecmp(argv[nopts],"-data",5) == 0) {
            if (++nopts >= argc) {
                printf("Need argument after -data \n");
                exit(1);
            }
            //cl_inputs -> data = (char*)malloc(sizeof(char)*strlen(argv[nopts])+1);
            found_arg = true;
            cl_inputs -> data = argv[nopts];
            nopts++;
            continue;
        }
        if (strncasecmp(argv[nopts],"-mode",5)==0) {
            if (++nopts >= argc) {
                printf("Need argument after -mode\n");
                exit(1);
            }
            found_arg = true;
            cl_inputs -> mode = argv[nopts];
            nopts++;
            continue;
        }
        if (strncasecmp(argv[nopts],"-predictor_model",16)==0) {
            if (++nopts >= argc) {
                printf("Need argument after -model_file\n");
                exit(1);
            }
            found_arg = true;
            cl_inputs->model_file = argv[nopts];
            nopts++;
            continue;
        }
        if (strncasecmp(argv[nopts], "-algorithm",10) == 0) {
            if (++nopts >= argc) {
                printf("Need argument after -algorithm\n");
                exit(1);
            }
            found_arg = true;
            cl_inputs -> algorithm = argv[nopts];
            nopts++;
            continue;
       }
       if (strncasecmp(argv[nopts], "-iterations", 11) == 0) {
           if (++nopts >= argc) {
               printf("Need argument after -iterations\n");
               exit(1);
           }
           found_arg = true;
           cl_inputs -> iterations = std::stoi(argv[nopts]);
           nopts++;
           continue;
       }
       if (strncasecmp(argv[nopts], "-pegasos_lambda", 15) == 0) {
           if (++nopts >= argc) {
               printf("Need argument after -pegasos_lambda\n");
               exit(1);
           }
           found_arg = true;
           //cl_inputs -> pegasos_lambda =stod(argv[nopts]);
           nopts++;
           continue;
       }
       if (!found_arg) {
           printf("No command line argument for %s\n",argv[nopts]);
           exit(1);
       }
    }
}

void help_syntax(void) {
    printf( 
	"\t\t\tProgram: Classify_Main\n"
	"\t\t\tAuthor: Darian Hadjiabadi\n"
	"\n"
	"\t\tusage: parameterss \n"
	"\t\t-algorithm <String> The name of the algorithm for training.\n"
	"\t\t-data <String> The data to use.\n"
	"\t\t-mode <String> Operating mode: train ortest.\n"
	"\t\t-model_file <String> The name of the model file to create/load.\n");

}


int main(int argc, char* argv[]) {
    // if no argument, shortcut to help
    if (argc == 1) { 
        help_syntax();
        exit(1);
    }
    CLInputs * cl_inputs = (CLInputs*) malloc(sizeof(CLInputs));
    read_opts(cl_inputs, argc, argv);

    if (argc < 2) { 
        printf("Too few aguments, use -help for information\n");
        exit(1);
    }
    if (strncasecmp(cl_inputs->mode,"train",5)==0) {
        if (cl_inputs -> data == NULL || cl_inputs -> model_file == NULL || cl_inputs -> algorithm == NULL) {
            printf("Training requires algorithm, data and, model_file\n");
            exit(1);
        }
        DataReader data_reader(cl_inputs -> data);
        vector<Instance> instances = data_reader.ReadData();
        if (strncasecmp(cl_inputs->algorithm,"majority",8)==0) {
            Predictor * predictor = new MajorityPredictor();
            train(predictor, instances);
        
            MajorityPredictor * mp = dynamic_cast<MajorityPredictor*>(predictor);
            std::ofstream ofs(cl_inputs->model_file);
            ofs << *mp;
            ofs.close();
            free_labels(instances);

        } else if (strncasecmp(cl_inputs->algorithm, "pegasos",7)==0) {
            Predictor * predictor = new SVMPredictor(cl_inputs->iterations, cl_inputs->pegasos_lambda);
            predictor->train(instances);
            SVMPredictor * svm = dynamic_cast<SVMPredictor*>(predictor);
            /*std::ofstream ofs(cl_inputs->model_file);
            ofs << *svm;
            ofs.close();
            free_labels(instances); */
        }
    } else if (strncasecmp(cl_inputs->mode,"test",4)==0) {
        if (cl_inputs -> data == NULL || cl_inputs -> model_file  == NULL || cl_inputs -> algorithm == NULL) {
            printf("Testing requires data and model_file\n");
            exit(1);
        }
        std::ifstream ifs(cl_inputs->model_file);
        if (strncasecmp(cl_inputs->algorithm,"pegasos",7) == 0) {
            SVMPredictor svm;
           /* if(ifs >> svm) {
	        DataReader data_reader(cl_inputs->data);
                vector<Instance> instances = data_reader.ReadData();
                Predictor * predictor = &svm;
                evaluateAndSavePredictions(predictor, instances); 
                free_labels(instances);
            } */
        } else if (strncasecmp(cl_inputs->algorithm,"majority", 8) == 0) {
              MajorityPredictor mp;
              if (ifs >> mp) {
                  DataReader data_reader(cl_inputs->data);
                  vector<Instance> instances = data_reader.ReadData();
                  Predictor * predictor = &mp;
                  evaluateAndSavePredictions(predictor, instances);
                  free_labels(instances);
             }
         } else{
         printf("Could not extract predictor\n");
            exit(1);
        }
    } else {
        printf("Mode must either be 'test' or 'train'\n");
        exit(1);
    }
    free(cl_inputs);
    return 0;
}

