

#include "Instance.h"
#include "Label.h"
#include "ClassificationLabel.h"
#include "FeatureVector.h"

Instance::Instance(FeatureVector fv, Label * lab) {
    this->label = lab;
    this->feature_vector = fv;
}

Instance::~Instance() {
}

Label* Instance::getLabel() {
    return this->label;
}

void Instance::setLabel(Label * new_lab) {
    this-> label = new_lab;
}

FeatureVector Instance::getFeatureVector() {
    return this-> feature_vector;
}

void Instance::setFeatureVector(FeatureVector new_fv) {
    this->feature_vector = new_fv;
}
