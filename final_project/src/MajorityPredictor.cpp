

#include "Predictor.h"
#include "MajorityPredictor.h"
#include "Label.h"
#include "ClassificationLabel.h"
#include "Instance.h"

#include <vector>
#include <string.h>
#include <fstream>
#include <iostream>

using std::vector;
using std::string;

MajorityPredictor::MajorityPredictor() {
    label_zero_count = 0;
    label_one_count = 0;
}
MajorityPredictor::~MajorityPredictor() {}

void MajorityPredictor::train(vector<Instance> instances) {
    for (unsigned int i = 0; i < instances.size(); i++) {
        Instance instance = instances[i];
        Label * label = instance.getLabel();
        string label_string = label->toString();
        int label_int = stoi(label_string);
        if (label_int == 0) {
            label_zero_count += 1;
        } else if (label_int == 1) {
            label_one_count +=1;
        }
    }
}

Label* MajorityPredictor::predict(Instance instance) {
    Label * pred_label = NULL;
    if (label_zero_count > label_one_count) {
        pred_label = new ClassificationLabel(0);
    } else {
        pred_label = new ClassificationLabel(1);
    }
    return pred_label;
}

int MajorityPredictor::get_zero_count(void) {
    return label_zero_count;
}

int MajorityPredictor::get_one_count(void) {
    return label_one_count;
}

void MajorityPredictor::set_zero_count(int zero) {
    label_zero_count = zero;
}

void MajorityPredictor::set_one_count(int one) {
    label_one_count  = one;
}

