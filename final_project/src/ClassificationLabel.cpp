

#include "ClassificationLabel.h"
#include "Label.h"
#include <string>

using std::string;
using std::to_string;


ClassificationLabel::ClassificationLabel(int label) {
    this -> label = label;
}

ClassificationLabel::~ClassificationLabel() {}

string ClassificationLabel::toString() {
    return to_string(this->label);
}
